function renderDSSV(dssv) {
  var contentHTML = "";
  for (var i = 0; i < dssv.length; i++) {
    var sv = dssv[i];
    var content = `<tr>
        <td>${sv.ma}</td>
        <td>${sv.ten}</td>
       <td>${sv.email}</td>
        <td>${sv.tinhDTB()}</td>
      <td> <button onclick="xoaSinhVien('${
        sv.ma
      }')" class="btn btn-danger">Xóa</button>
      <button onclick="suaSinhVien('${
        sv.ma
      }')" class="btn btn-success">Sửa</button> </td>
        </tr>`;
    contentHTML = contentHTML + content;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}
function showThongtinlenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan * 1;
  document.getElementById("txtDiemLy").value = sv.ly * 1;
  document.getElementById("txtDiemhoa").value = sv.hoa * 1;
}
function layThongtinTuForm() {
  var ma = document.getElementById("txtMaSV").value
  var ten = document.getElementById("txtTenSV").value
  var email = document.getElementById("txtEmail").value
  var matKhau = document.getElementById("txtPass").value
  var toan = document.getElementById("txtDiemToan").value*1
  var ly = document.getElementById("txtDiemLy").value*1
  var hoa = document.getElementById("txtDiemHoa").value*1
  var sv = new Sinhvien(ma,ten,email,matKhau,toan,ly,hoa)
  return sv
}
